package com.analystplatform.seatbooker;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.analystplatform.seatbooker.models.SeatHold;
import com.analystplatform.seatbooker.services.SeatBookingService;

public class SeatBookingEngineTest {

	private SeatBookingEngine engine;
	// counter of tests
	private int testCounter = 0;
			
	// counter of correct tests
	private int correctTests = 0;
			
	//test string to use
	private String testEmailName = "heitmaddy@gmail.com";

	
	@Before
	public void setup() {
		engine = new SeatBookingEngine();
	}
	
	
	
	@Test
	public void testTheSeatHold() {
		try {
			engine.findAndHoldSeats((ApplicationConfig.COLUMNS-2), testEmailName);
			System.out.println("Test - Hold Seats - \u2713");
			correctTests++;
			

		}
		catch(Exception e){
			System.out.println("Test - Hold Seats - FAILED");
			Assert.fail("FAIL");
		}
	}
	
	@Test
	public void testTheSeatHoldAndReservation() {
		try {
			SeatHold sh = engine.findAndHoldSeats((ApplicationConfig.COLUMNS-2), testEmailName);
			engine.reserveSeats(sh.getId(), sh.getEmail());
			System.out.println("Test - Hold Seats - \u2713");
			correctTests++;
			

		}
		catch(Exception e){
			System.out.println("Test - Hold Seats - FAILED");
			Assert.fail("FAIL");
		}
	}
	

	
	@Test
	public void testAddMoreSeatsThanRow() {
		testCounter++;
		// test to see if you ask for more seats than are in a single row
		try {
			engine.findAndHoldSeats(ApplicationConfig.COLUMNS+10, testEmailName);
			System.out.println("Test - Cannot hold seats of size larger than column length - \u2713");
			correctTests++;
		}
		catch(Exception e) {
			System.out.println("Test - Cannot hold seats of size larger than column length - FAILED");
			Assert.fail("FAIL");
		}
	}
	
	@Test
	public void testReservingASeatWIthUnvalidEmail() {
		testCounter++;
		// test to see if you can reserve a seat with an unvalid email
		try {
			SeatHold seatHoldTest0 = engine.findAndHoldSeats(10, testEmailName);
			String testString = engine.reserveSeats(seatHoldTest0.getId(), seatHoldTest0.getEmail());
			if(testString!= SeatBookingService.HOLD_FOR_DIFFERENT_CUSTOMER_MESSAGE) {
				System.out.println("Test - unvalid confirmation email - \u2713"); 
				correctTests++;
			}
			else {
				System.out.println("Test - unvalid confirmation email - FAILED");
				Assert.fail("FAIL");
			}
		}
		catch(Exception e) {
			System.out.println("Test - unvalid confirmation email - FAILED");
			Assert.fail("FAIL");
		}
	}
	
	@Test
	public void testReservaingSeatWithNonvalidConfirmationNumber() {
		testCounter++;
		// test to see if you can reserve a seat with an unvalid confirmation number
		try {
			SeatHold seatHoldTest0 = engine.findAndHoldSeats(10, testEmailName);
			String testString = engine.reserveSeats(1, seatHoldTest0.getEmail());
			System.out.println("testString: "+ testString);
			if(testString == SeatBookingService.HOLD_NOT_FOUND_MESSAGE) {
				System.out.println("Test - unvalid confirmation code - \u2713"); 
				correctTests++;
		}
			else {
				System.out.println("Test - unvalid confirmation code - FAILED");
				Assert.fail("FAIL");
			}
		}
		catch(Exception e) {
			System.out.println("Test - unvalid confirmation code - FAILED");
			Assert.fail("FAIL");
		}
	}

	@Test
	public void testIfYouCanReserveSeatWithValidCreds() {
		testCounter++;
		// test to see if you can reserve a seat with valid creds
		try {
			SeatHold seatHoldTest0 = engine.findAndHoldSeats(10, testEmailName);
			String testString = engine.reserveSeats(seatHoldTest0.getId(), seatHoldTest0.getEmail());
			System.out.println("Test - Reserve a seat with valid Creds"+ testString + " - \u2713"); 
				System.out.println("Test - valid creds - \u2713"); 
				correctTests++;
		}
		catch(Exception e) {
			System.out.println("Test - valid creds - FAILED");
			Assert.fail("FAIL");
		}
	}
	
	@Test
	public void testReservingANewLine() {
		testCounter++;
		// test to see if you can reserve a seat when you will need a new line
		try {
			SeatHold seatHoldTest0 = engine.findAndHoldSeats(ApplicationConfig.COLUMNS-2, testEmailName);
			engine.reserveSeats(seatHoldTest0.getId(), seatHoldTest0.getEmail());
			System.out.println("Test - Reserve a seat with valid Creds on a new line - \u2713"); 
			correctTests++;
		}
		catch(Exception e) {
			System.out.println("Test - Reserve a seat with valid Creds on a new line - FAILED"); 
			Assert.fail("FAIL");
		}
	}
	
	@Test
	public void testTryingToBookMoreSeatsThanAreAvailable() {

		testCounter++;
		// test to see if you can book more seats than are available
		try {
			
			for(int i = 0; i< ApplicationConfig.ROWS+1 ; i++) {
				SeatHold testSeatHold = engine.findAndHoldSeats(ApplicationConfig.COLUMNS-1,testEmailName);
				engine.reserveSeats(testSeatHold.getId(), testEmailName);
			}
			System.out.println("Test - Try to book more seats than are available - FAILED");
			Assert.fail("FAIL");
		}
		catch(Exception e) {
			System.out.println("Test - Try to book more seats than are available - \u2713");
			correctTests++;
		}
	}
	
	
	
	
	public void testing() {
	
		
		
		System.out.println("SeatBooking Engine Test: START");
		System.out.println("--------------------------------");
		System.out.println("--------------------------------");
		
		//calling all of the tests
		testTheSeatHold();
		testAddMoreSeatsThanRow();
		testReservingASeatWIthUnvalidEmail();
		testReservaingSeatWithNonvalidConfirmationNumber();
		testIfYouCanReserveSeatWithValidCreds();
		testReservingANewLine();
		testTheSeatHoldAndReservation();
		testTryingToBookMoreSeatsThanAreAvailable();
		

		
		// results
		System.out.println("--------------------------------");
		System.out.println("--------------------------------");
		System.out.println("Test Result Overview - Tests: "+ testCounter);
		System.out.println("Passed: " + correctTests);
		System.out.println("Failed: " + (testCounter-correctTests));
		System.out.println("--------------------------------");
		System.out.println("SeatBooking Engine Test: END");
		
		
	}

	
	
	
	
}
