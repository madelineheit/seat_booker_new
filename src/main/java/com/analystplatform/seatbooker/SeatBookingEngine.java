package com.analystplatform.seatbooker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


import com.analystplatform.seatbooker.models.ReservationConfirmation;
import com.analystplatform.seatbooker.models.Seat;
import com.analystplatform.seatbooker.models.SeatHold;
import com.analystplatform.seatbooker.services.SeatBookingService;


/**
 * TODO: Implement the SeatBookingEngine according to the instructions in the Readme.
 * 
 * Model classes already exist for Seat, SeatHold, and ReservationConfirmation.  Their definition and javadoc are included in the classpath jar.
 * 
 * All seat interactions should be achievable via the parent class's included SeatDao reference.  It can be accessed within SeatBookingEngine via:
 * 
 * <pre>
 * this.getDao()
 * </pre>
 * 
 * The public methods on SeatDao include the following list. 
 * 
 * <pre>
 * // Returns all the seats 
 * public Seat[][] getSeats();
 * 
 * // Returns the number of rows in the venue
 * public int getNumberOfRows();
 * 
 * // Returns the number of columns in the venue
 * public int getNumberOfColumns();
 * 
 * // Get all the Seats being held, mapped to the IDs of their holds
 * public Map<Integer, SeatHold> getSeatHolds();
 * 
 * // Removes all the seat holds of the given IDs
 * public void removeAllSeatHolds(List<Integer> seatHoldIds);
 * 
 * // Holds a list of Seats for the given customer email address. Creates IDs and updates the in-memory data model with
 * // the held seats
 * public SeatHold newSeatHold(String customerEmail, List<Seat> seatsToHold);
 * 
 * // Returns a seat hold for the given ID. Returns null if one wasn't found
 * public SeatHold getSeatHold(int id);
 * 
 * // Removes a seat hold from the database of seat holds
 * public void removeSeatHold(int id);
 * 
 * // Returns any expired seats into the pool of available seats.
 * public void reclaimExpiredHolds();
 * 
 * // Returns the given confirmation for a valid id, null otherwise
 * public ReservationConfirmation getReservationConfirmation(String confirmationNumber);
 * 
 * // Saves the given confirmation
 * public void saveReservationConfirmation(ReservationConfirmation confirmation);
 * 
 * // Returns all reservation confirmations as a list
 * public List<ReservationConfirmation> getReservationConfirmations();
 * </pre>
 * 
 */
public class SeatBookingEngine extends SeatBookingService {

	/**
	 * The number of seats that are neither held nor reserved
	 *
	 * @return the number of seats available in the venue
	 */
	@Override
	public int numSeatsAvailable() {
		int counterForOpenSeats = 0;
		for(int i=0; i<getDao().getNumberOfRows(); i++) {
			for(int j=0; j<getDao().getNumberOfColumns(); j++) {
				// if the seat is open, then add to counter
				// Note*  in the Seat class that isOpen(true) == !(isHeld || isReserved)
				if(getDao().getSeats()[i][j].isOpen() == true) {
					counterForOpenSeats++;
				} //end if seats is open
			} // end column
		} //end row
		// return the total number of open seats
		return counterForOpenSeats;
	}

	

	/**
	 * Find and hold the best available seats for a customer.
	 * <p>
	 * Note: Customers who book together prefer to sit near each other whenever possible.  Seats are preferable nearest the stage.
	 * </p>
	 * @param numSeats      the number of seats to find and hold
	 * @param customerEmail unique identifier for the customer
	 * @return a SeatHold object identifying the specific seats and related information
	 */
	@Override
	public SeatHold findAndHoldSeats(int numSeats, String customerEmail) {
		// TODO: Leverage the underlying DAO to perform this operation
		// for the numOfSeats and the customerEmail need to 
		
		
		// if the number is too small
		if(numSeats <= 0) {
			System.out.println("Please enter a positive number of seats greater than zero.");
			throw new RuntimeException("Please enter a positive number of seats greater than zero.");
		}
		// if the party size requested is larger than the number of seats available
		else if(numSeats > numSeatsAvailable()) {
			System.out.println("There are not enough seats available to hold your whole party. Please try a smaller group size.");
			throw new RuntimeException("There are not enough seats available to hold your whole party. Please try a smaller group size.");
		}
		// when the party size is a correct value
		else {
			
			// add the list of seats to a seat hold
			SeatHold sh = new SeatHold();
			List<Seat> seatsHeld = seatListFromPartySize(numSeats);
			sh.setSeats(seatsHeld);
			
			// call a new seat hold to get the seat hold id
			SeatHold seatHoldGetID = getDao().newSeatHold(customerEmail, seatsHeld);
			
			// with the seat hold id alert the user of seat hold information
			alertUserOfSeatsHeld(sh, seatHoldGetID.getId(), customerEmail, seatsHeld);
			return sh;

		}
	}



	
	private void alertUserOfSeatsHeld(SeatHold sh, int id, String customerEmail, List<Seat> seatsHeld) {
		// updating the SeatHold info
		sh.setId(id);
		sh.setEmail(customerEmail);
		sh.setSeats(seatsHeld);

				
		//60 seconds converted to long is seconds*100 with an 'l' at the end
		sh.setExpirationTime( System.currentTimeMillis() + 60000l);
				
		// Printing out the seat hold ID in case the user wants to reserve the seat
		System.out.println("\n\n---------------------- Attention! ---------------------");
		System.out.println("Your seats have been held, but NOT reserved.");
		System.out.println("You have 60 seconds to set your held seats to reserved.");
		System.out.println("Please use the hold id to reserve your seats: " + id);

						
				
		// after 60 seconds check to see if the seats have been reserved
		// if the seats have not been reserved after 60 seconds, then a message will appear and
		//     the seats will be reinitialized to open
		final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

		final Runnable task = new Runnable() {
			public void run() {
				   // when the 60 seconds have run out
				  if( getDao().getSeatHolds().containsKey(id) == true) {
				    System.out.println("Time has ended to reserve held Seat Hold ID: " + id);
				    // remove it from the map
				    getDao().removeSeatHold(id);
				    changeAHeldSeatBackToAvailable(sh.getSeats());
				    }
				   }
			};
			// to run after 60 seconds
			final ScheduledFuture<?> handle = scheduler.scheduleAtFixedRate(task, 60, 60, TimeUnit.SECONDS);
			scheduler.schedule(new Runnable() {
				public void run() { 
				    handle.cancel(true); 
				}
			}, 60, TimeUnit.SECONDS);
	
	}



	private void changeAHeldSeatBackToAvailable(List<Seat> seats) {

		// go through the seats and assign the seats to be released back to available
		for(Seat releaseSeat : seats) {
		
			releaseSeat.setColumn(releaseSeat.getColumn());
			releaseSeat.setRow(releaseSeat.getRow());
			releaseSeat.setReserved(false);
			releaseSeat.setHeld(false);
			
			getDao().getSeats()[releaseSeat.getRow()][releaseSeat.getColumn()].setHeld(false);
			getDao().getSeats()[releaseSeat.getRow()][releaseSeat.getColumn()].setReserved(false);
			
		}
		
	}

	private void changeOpenSeatToHeld(List<Seat> seats) {
		
		// go through the seats and assign the seats to be held
		for(Seat holdSeat : seats) {
			
			holdSeat.setColumn(holdSeat.getColumn());
			holdSeat.setRow(holdSeat.getRow());
			holdSeat.setReserved(false);
			holdSeat.setHeld(true);
			
			getDao().getSeats()[holdSeat.getRow()][holdSeat.getColumn()].setHeld(true);
			getDao().getSeats()[holdSeat.getRow()][holdSeat.getColumn()].setReserved(false);
		}
		
	}

	
	private List<Seat> seatListFromPartySize(int numSeats){
		// create a list of seats and the counter to make sure that 
		// the correct number of seats are held
		List<Seat> listOfSeats =  new ArrayList<Seat>();
		int counterOfOpenSeats = 0;
		
		// go through rows and columns
		for(int row = 0; row < getDao().getNumberOfRows(); row++) {
			for(int col = 0; col < getDao().getNumberOfColumns(); col++) {
				// when the seat is open
				// add the seat to the seat array
				// and increase the counter
				if(getDao().getSeats()[row][col].isOpen() == true) {
					Seat newSeat = new Seat();
					newSeat.setRow(row);
					newSeat.setColumn(col);
					listOfSeats.add(newSeat);
					counterOfOpenSeats++;
				}
				// when enough seats have been held to account for the party size
				// return the list of seats
				if(counterOfOpenSeats==numSeats) {
					changeOpenSeatToHeld(listOfSeats);
					return listOfSeats;
				}
			}
		}
		
		return listOfSeats;
	}
	
		

	/**
	 * Reserve unexpired seats held for a specific customer
	 * 
	 * Implementation note: 
	 * 
	 * <pre>
	 * - If a 'hold' cannot be found for a given seatHoldId, return SeatBookingService.HOLD_NOT_FOUND_MESSAGE
	 * - If a 'hold' doesn't exist for the given customerEmail, return SeatBookingService.HOLD_FOR_DIFFERENT_CUSTOMER_MESSAGE
	 * </pre>
	 * 
	 *
	 * @param seatHoldId    the seat hold identifier
	 * @param customerEmail the email address of the customer to which the seat hold is assigned
	 * @return a reservation confirmation code
	 */
	@Override
	public String reserveSeats(int seatHoldId, String customerEmail) {
		
		// if the hold id is not in the hash map of seat holds
		if(getDao().getSeatHolds().containsKey(seatHoldId)== false) {
			return SeatBookingService.HOLD_NOT_FOUND_MESSAGE;
		}
		// is the key exists, see if the emails match
		else {
			// if the emails match, then reserve the seats and return the confirmation ID
			// Note* == tests object references, .equals() tests the string values.
			if (getDao().getSeatHold(seatHoldId).getEmail().equals(customerEmail)) {
				updateTheHeldSeatsToReservedSeats(seatHoldId, customerEmail, getDao().getSeatHold(seatHoldId).getSeats());
				return Integer.toString(seatHoldId);
			}
			// if the emails don't match
			else {
				return SeatBookingService.HOLD_FOR_DIFFERENT_CUSTOMER_MESSAGE;
			}
		} // end else if the key exists
		
	}

	private void updateTheHeldSeatsToReservedSeats(int seatHoldId, String customerEmail, List<Seat> seats) {
		
		//create a reservation and set the values
		ReservationConfirmation reserve = new ReservationConfirmation();
		reserve.setEmail(customerEmail);
		reserve.setId(Integer.toString(seatHoldId));
		reserve.setSeats(seats);

		//set all of the seats to be reserved
		for(Seat reserveSeat : seats) {
			
			reserveSeat.setColumn(reserveSeat.getColumn());
			reserveSeat.setRow(reserveSeat.getRow());
			reserveSeat.setReserved(true);
			reserveSeat.setHeld(false);
			
			getDao().getSeats()[reserveSeat.getRow()][reserveSeat.getColumn()].setHeld(false);
			getDao().getSeats()[reserveSeat.getRow()][reserveSeat.getColumn()].setReserved(true);
			
		}

		//saves the given set confirmation
		getDao().saveReservationConfirmation(reserve);	

		// lastly need to remove the SeatHold key value pair in the getDao hash map
		getDao().removeSeatHold(seatHoldId);
	}
	
} //end of updateTheHeldSeatsToReservedSeats

